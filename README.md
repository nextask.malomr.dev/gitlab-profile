# NexTask 

NexTask is a project management app which focuses a lot on ease of use and user experience.

---

[[_TOC_]]

## Structure

The app is divided in two parts.

- The [API](https://gitlab.com/nextask.malomr.dev/api) using :
    - Java
    - SpringBoot
    - MySQL
    - Maven
- The [App](https://gitlab.com/nextask.malomr.dev/app) using :
    - Flutter
    - Dart
- The [Website](https://gitlab.com/nextask.malomr.dev/web) using :
    - Vue.js

The use of Flutter allows a single codebase to be deployed on Android and IOS seemlessly.

Having the API strictly separated from the app allows for easier maintenance and allows the user to switch platforms while keeping the same account.

_**By @malomr**_
